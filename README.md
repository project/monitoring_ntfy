# Monitoring ntfy.sh Integration module

## Features

This module integrates ntfy.sh with the Monitoring module and will send
notifications to a configured topic when the status of a certain sensor will
change. Currently, you can select one or more severities which will trigger a
notification. This is on a global level, setting this up individually for each
sensor might be done later.

## Installation

Install as usual, recommend is to install via composer.
This module uses the [ntfy-php-library][library-link] from github.

## Usage

After installing the module you need configure the notification topic and the
severities that triggers notifications. The settings page can be found at
*/admin/config/system/monitoring/settings/ntfy*.


[library-link]: https://github.com/VerifiedJoseph/ntfy-php-library
