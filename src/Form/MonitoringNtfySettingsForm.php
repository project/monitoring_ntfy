<?php

namespace Drupal\monitoring_ntfy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\monitoring\Entity\SensorResultDataInterface;

/**
 * Provide the configuration form for the NtfyService.
 */
class MonitoringNtfySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monitoring_ntfy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['monitoring_ntfy.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('monitoring_ntfy.settings');
    $config_ntfy = $this->config('ntfy.settings');

    // Configure the default topic for sending notifications.
    $form['monitoring_ntfy_default_topic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification Topic'),
      '#default_value' => $config->get('monitoring_ntfy_default_topic'),
      '#placeholder' => $config_ntfy->get('ntfy_default_topic') ?? '',
      '#description' => $this->t('Enter the default topic to send monitoring notifications to. If left empty, the default topic configured at the <a href=":ntfy.settings">ntfy.sh settings page</a> will be used.', [':ntfy.settings' => Url::fromRoute('ntfy.ntfy_settings')->toString()]),
    ];

    // Allow the user to choose on which severities to send a notification.
    $severities = [
      SensorResultDataInterface::STATUS_OK => $this->t('OK'),
      SensorResultDataInterface::STATUS_WARNING => $this->t('Warning'),
      SensorResultDataInterface::STATUS_CRITICAL => $this->t('Critical'),
      SensorResultDataInterface::STATUS_INFO => $this->t('Info'),
      SensorResultDataInterface::STATUS_UNKNOWN => $this->t('Unknown'),
    ];
    $form['monitoring_ntfy_severity_triggers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Severity Triggers'),
      '#description' => $this->t('Select the severities which trigger a notification on change.'),
      '#options' => $severities,
      '#default_value' => $config->get('monitoring_ntfy_severity_triggers') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    // Set ntfy settings.
    $this->config('monitoring_ntfy.settings')
      ->set('monitoring_ntfy_default_topic', $form_state->getValue('monitoring_ntfy_default_topic'))
      ->set('monitoring_ntfy_severity_triggers', $form_state->getValue('monitoring_ntfy_severity_triggers'))
      ->save();
  }

}
